package com.example.labo2;

import android.graphics.Bitmap;
import android.graphics.Matrix;

public class AstreCeleste {
    private int id,tailleAstre;
    private String nomAstre,couleurAstre,nomImageAstre;
    private Boolean statusAstre;

    public AstreCeleste(int id, String nomAstre, int tailleAstre, String couleurAstre, Boolean statusAstre, String nomImageAstre) {
        this.id = id;
        this.tailleAstre = tailleAstre;
        this.nomAstre = nomAstre;
        this.couleurAstre = couleurAstre;
        this.nomImageAstre = nomImageAstre;
        this.statusAstre = statusAstre;
    }
    public AstreCeleste(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTailleAstre() {
        return tailleAstre;
    }

    public void setTailleAstre(int tailleAstre) {
        this.tailleAstre = tailleAstre;
    }

    public String getNomAstre() {
        return nomAstre;
    }

    public void setNomAstre(String nomAstre) {
        this.nomAstre = nomAstre;
    }

    public String getCouleurAstre() {
        return couleurAstre;
    }

    public void setCouleurAstre(String couleurAstre) {
        this.couleurAstre = couleurAstre;
    }

    public String getNomImageAstre() {
        return nomImageAstre;
    }

    public void setNomImageAstre(String nomImageAstre) {
        this.nomImageAstre = nomImageAstre;
    }

    public Boolean getStatusAstre() {
        return statusAstre;
    }

    public void setStatusAstre(Boolean statusAstre) {
        this.statusAstre = statusAstre;
    }
    public Bitmap getResizedBitmap(Bitmap img, int newWigth, int newHeight){
        int width = img.getWidth();
        int height = img.getHeight();
        float scaleWigth = ((float)newWigth)/width;
        float scaleHeight = ((float)newHeight)/height;

        Matrix matrix = new Matrix();

        matrix.postScale(scaleWigth,scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(
                img,0,0,width,height,matrix,false);
        img.recycle();
        return  resizedBitmap;
    }
}