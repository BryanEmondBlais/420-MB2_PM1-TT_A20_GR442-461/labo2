package com.example.labo2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class AlienSolarSystem extends View {
    private Random alea;
    private int planetX,planetY;
    private int cnt;
    private int screenW,screenH;
    private astre[] astres = new astre[8];
    private Context mcontext;
    private boolean fin;
    private Bitmap resizedImage;

    @Override
    protected void onSizeChanged(int w,int h,int oldW, int oldH){
        super.onSizeChanged(w,h,oldW,oldH);
        screenH =h;
        screenW =w;
    }

    public AlienSolarSystem(Context context) {
        super(context);
        mcontext = context;
        fin = false;
        cnt =0;
        alea = new Random();
        Bitmap v = BitmapFactory.decodeResource(mcontext.getResources(),R.drawable.vaisseau);
        resizedImage = Bitmap.createScaledBitmap(v,200,200,true);
        planetX = alea.nextInt(1000);
        planetY = alea.nextInt(1000);

        for(int i = 0; i<8;i++){
            astre temp= new astre();
            astres[i] = temp;
        }
    }

    @Override
    protected void onDraw(Canvas canvas){
        Bitmap blackscreen = BitmapFactory.decodeResource(mcontext.getResources(),R.drawable.hqdefault);
        Bitmap blackS = Bitmap.createScaledBitmap(blackscreen,screenW,screenH,true);
        canvas.drawBitmap(blackS,0,0,null);

        MyDBAdaptater myDBAdapter = new MyDBAdaptater(mcontext.getApplicationContext());
        myDBAdapter.Open();
        ArrayList<AstreCeleste> ls = new ArrayList<AstreCeleste>();
        ls = myDBAdapter.selectAstre();

        try{
            for(int i=0; i<8;i++){
                int image = mcontext.getResources().getIdentifier(ls.get(i).getNomImageAstre(),"drawable",mcontext.getPackageName());
                Bitmap imageSelect = BitmapFactory.decodeResource(mcontext.getResources(),image);
                Bitmap resizedImage2 = Bitmap.createScaledBitmap(imageSelect,ls.get(i).getTailleAstre(),ls.get(i).getTailleAstre(),true);
                astres[i].onDraw(canvas,resizedImage2,ls.get(i).getTailleAstre());
            }
            if(cnt >= 8 && !fin){
                Toast.makeText(mcontext, "La partie est terminee", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception ex){
            System.out.println(ex);
        }
        canvas.drawBitmap(resizedImage,planetX-100,planetY-100,null);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){

        MyDBAdaptater myDBAdapter = new MyDBAdaptater(mcontext.getApplicationContext());
        myDBAdapter.Open();
        ArrayList<AstreCeleste> ls = new ArrayList<AstreCeleste>();
        ls = myDBAdapter.selectAstre();

        int action = event.getAction();
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();

        boolean limitL,limitR,limitU,limitD = false;

        switch (action){
            case MotionEvent.ACTION_MOVE:
                planetX = touchX;
                planetY = touchY;

                for(int i = 0; i < 8; i++){
                    limitL = planetX > (astres[i].getPosX()-30);
                    limitR =  planetX < (astres[i].getPosX()+ls.get(i).getTailleAstre());
                    limitU =  planetY > (astres[i].getPosY()-30);
                    limitD =  planetY < (astres[i].getPosY()+ls.get(i).getTailleAstre());
                    if(limitL && limitR && limitD && limitU )
                    {
                        if(astres[i].getStatus()){
                            astres[i].setStatus(false,ls.get(i).getStatusAstre(),ls.get(i).getNomAstre(),mcontext);
                            cnt++;
                        }
                        System.out.println(cnt);
                    }
                }
                break;
        }
        invalidate();
        return true;
    }

}
