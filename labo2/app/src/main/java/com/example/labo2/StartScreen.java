package com.example.labo2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StartScreen extends View {
    private int screenW,screenH;
    private Bitmap startPageLogo,btnMoveUp,btnMoveDown,btnAccUp,btnAccDown;
    private  boolean playBtnMove,playBtnAcc;
    private Context myContext;
    private String[] astreNom,couleur,imageAstre;
    private Boolean[] status;

    @Override
    protected void onSizeChanged(int w,int h,int oldW, int oldH){
        super.onSizeChanged(w,h,oldW,oldH);
        screenH =h;
        screenW =w;
    }

    public StartScreen(Context context){
        super(context);
        myContext = context;
        startPageLogo = BitmapFactory.decodeResource(getResources(),R.drawable.astre);
        btnMoveUp = BitmapFactory.decodeResource(getResources(),R.drawable.touchscreen);
        btnMoveDown = BitmapFactory.decodeResource(getResources(),R.drawable.touchscreen3);
        btnAccUp = BitmapFactory.decodeResource(getResources(),R.drawable.acc2);
        btnAccDown = BitmapFactory.decodeResource(getResources(),R.drawable.acc3);

        MyDBAdaptater myDBAdapter = new MyDBAdaptater(myContext.getApplicationContext());
        myDBAdapter.Open();
        astreNom = new String[8];
        astreNom[0] = "Neptune";
        astreNom[1] = "Uranus";
        astreNom[2] = "Mars";
        astreNom[3] = "Venus";
        astreNom[4] = "Mercure";
        astreNom[5] = "Jupiter";
        astreNom[6] = "Saturn";
        astreNom[7] = "pluto";

        couleur = new String[4];
        couleur[0] = "Noire";
        couleur[1] = "Rouge";
        couleur[2] = "Bleu";
        couleur[3] = "Jaune";

        imageAstre = new String[8];
        imageAstre[0] = "planet1";
        imageAstre[1] = "planet2";
        imageAstre[2] = "planet3";
        imageAstre[3] = "planet4";
        imageAstre[4] = "planet5";
        imageAstre[5] = "planet6";
        imageAstre[6] = "planet7";
        imageAstre[7] = "planet8";

        status = new Boolean[2];
        status[0] = true;
        status[1] = false;

        Random rand = new Random();
        ArrayList<AstreCeleste> ls = new ArrayList<AstreCeleste>();
        ls = myDBAdapter.selectAstre();
        if(ls.isEmpty()){

            for(int i = 0; i < 8; i++) {
                int randColor = rand.nextInt(4);
                int tailleAstre = rand.nextInt(500);
                boolean statusId = rand.nextBoolean();
                myDBAdapter.insertAstre(astreNom[i], tailleAstre,couleur[randColor], statusId,imageAstre[i]);
            }
        }else{
            myDBAdapter.deleteAstre();
            for(int i = 0; i < 8; i++) {
                int randColor = rand.nextInt(4);
                int tailleAstre = rand.nextInt(400)+100;
                boolean statusId = rand.nextBoolean();
                myDBAdapter.insertAstre(astreNom[i], tailleAstre, couleur[randColor], statusId, imageAstre[i]);
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas){
        startPageLogo = getResizedBitmap(startPageLogo,screenW,screenW*9/16);
        btnAccUp = getResizedBitmap(btnAccUp,screenW/2,screenW/2);
        btnAccDown = getResizedBitmap(btnAccDown,screenW/2,screenW/2);
        btnMoveUp = getResizedBitmap(btnMoveUp,screenW/2,screenW/2);
        btnMoveDown = getResizedBitmap(btnMoveDown,screenW/2,screenW/2);


        canvas.drawBitmap(startPageLogo,0,(int)(screenH*0.10),null);
        if(playBtnMove){
            canvas.drawBitmap(btnMoveDown,(screenW-btnMoveDown.getWidth()),(int)(screenH-btnMoveDown.getHeight()),null);
        }
        else{
            canvas.drawBitmap(btnMoveUp,(screenW-btnMoveUp.getWidth()),(int)(screenH-btnMoveUp.getHeight()),null);
        }
        if(playBtnAcc){
            canvas.drawBitmap(btnAccDown,0,(int)(screenH-btnAccDown.getHeight()),null);
        }
        else {
            canvas.drawBitmap(btnAccUp,0,(int)(screenH-btnAccUp.getHeight()),null);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        int action = event.getAction();
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();

        switch (action){
            case MotionEvent.ACTION_DOWN:
                if((touchX > (screenW/2)
                    && touchX < (screenW))
                    && ((touchY > (int)(screenH*0.75))
                    && (touchY < ((int)(screenH*0.75) + btnMoveUp.getWidth())))) {
                    playBtnMove = true;
                }
                else if((touchX > 0
                    && touchX < (screenW/2))
                    &&((touchY > ((int)(screenH*0.75))
                    &&(touchY < ((int)(screenH*0.75) + btnAccUp.getWidth()))))){
                    playBtnAcc = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if(playBtnMove){
                    Intent gameIntent = new Intent(myContext,moveActivity.class);
                    myContext.startActivity(gameIntent);
                }
                else if(playBtnAcc){
                    Intent gameIntent = new Intent(myContext,AccActivity.class);
                    myContext.startActivity(gameIntent);
                }
                playBtnAcc = false;
                playBtnMove= false;
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        invalidate();
        return true;
    }

    public Bitmap getResizedBitmap(Bitmap img, int newWigth, int newHeight){
        int width = img.getWidth();
        int height = img.getHeight();
        float scaleWigth = ((float)newWigth)/width;
        float scaleHeight = ((float)newHeight)/height;

        Matrix matrix = new Matrix();

        matrix.postScale(scaleWigth,scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(
                img,0,0,width,height,matrix,false);
        img.recycle();
        return  resizedBitmap;
    }

}
