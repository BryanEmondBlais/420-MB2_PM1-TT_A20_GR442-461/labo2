package com.example.labo2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDBHelper extends SQLiteOpenHelper {
    public MyDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE IF NOT EXISTS SolarSystem (id integer primary key autoincrement, nomAstre text, tailleAstre integer, couleurAstre text, statusAstre boolean, nomImageAstre text);";
        //String query = "DROP TABLE IF EXISTS SolarSystem;";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS SoclarSystem;";
        db.execSQL(query);
        onCreate(db);

    }
}
