package com.example.labo2;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class astre {
    private Canvas canvas2;
    private int posX,posY;
    private Paint crayon;
    private Random alea;
    private int radius;
    private  boolean status;
    AstreCeleste img;

    public astre(){
        status = true;
        alea = new Random();
        posX = alea.nextInt(1000);
        posY = alea.nextInt(1000);

        crayon = new Paint();
        crayon.setAntiAlias(true);
        crayon.setColor(Color.BLACK);

        radius =30;
    }
    public boolean getStatus(){return this.status;}

    public void setStatus(Boolean state,boolean statusAstre,String nom,Context context){
        this.status = state;
        if(!this.status){
            if(statusAstre){
                Toast.makeText(context, nom+" est habiter", Toast.LENGTH_LONG).show();
            }
            else{
                crayon.setColor(Color.TRANSPARENT);
            }
        }
    }

    public int getPosX(){return this.posX;}
    public int getPosY(){return this.posY;}

    protected void onDraw(Canvas canvas, Bitmap image,int rad){
//        radius = rad/2;
//        canvas2 = canvas;
//        canvas2.drawCircle(posX,posY,radius,crayon);
        canvas.drawBitmap(image,posX,posY,crayon);

    }
}