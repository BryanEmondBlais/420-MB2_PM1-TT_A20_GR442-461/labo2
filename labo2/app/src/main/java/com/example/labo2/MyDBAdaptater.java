package com.example.labo2;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class MyDBAdaptater {
    private Context context;
    private final String DATABASE_NAME= "SolarSystemBD";
    private MyDBHelper dbHelper;
    private final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;

    public MyDBAdaptater(Context context){
        this.context =context;
        this.dbHelper = new MyDBHelper(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    public void Open(){this.db = this.dbHelper.getWritableDatabase();}

    public void insertAstre(String nomAstre,int tailleAstre,String couleurAstre,Boolean statusAstre,String nomImageAstre){
        this.db.execSQL("INSERT INTO SolarSystem(nomAstre,tailleAstre,couleurAstre,statusAstre,nomImageAstre) VALUES('"+nomAstre+"','"+tailleAstre+"','"+couleurAstre+"','"+statusAstre+"','"+nomImageAstre+"');");
    }
    public ArrayList<AstreCeleste> selectAstre(){
        ArrayList<AstreCeleste> listAstre = new ArrayList<AstreCeleste>();
        Cursor cursor = this.db.rawQuery("SELECT * FROM SolarSystem",null);
        if((cursor != null) && cursor.moveToFirst()){
            do{
                listAstre.add(new AstreCeleste(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),Boolean.parseBoolean(cursor.getString(4)),cursor.getString(5)));
            }while(cursor.moveToNext());
        }
        return listAstre;
    }

    public void deleteAstre(){
        this.db.execSQL("DELETE FROM  SolarSystem;");
    }

}