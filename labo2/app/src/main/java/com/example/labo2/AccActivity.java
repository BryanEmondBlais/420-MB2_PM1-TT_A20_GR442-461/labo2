package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class AccActivity extends AppCompatActivity {

    AlienSolarSystem2 AlienSolarSystem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlienSolarSystem = new AlienSolarSystem2(this);

        setContentView(AlienSolarSystem);
    }
}