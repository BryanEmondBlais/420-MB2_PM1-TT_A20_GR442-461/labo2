package com.example.labo2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class moveActivity extends AppCompatActivity {

    AlienSolarSystem AlienSolarSystem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AlienSolarSystem = new AlienSolarSystem(this);

        setContentView(AlienSolarSystem);

    }
}